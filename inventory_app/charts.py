from django.http import JsonResponse
from .models import SalesOrder, Product
from chartjs.views.columns import BaseColumnsHighChartsView
from chartjs.views.lines import BaseLineChartView
from django.http import JsonResponse
from datetime import timedelta

#KEEP ALL CHART GENERATION FUNCTIONALITY HERE
import pdb

class TopTurnOverProductsBarchart(BaseColumnsHighChartsView):
    title = "Top 10 Products By Turnover"
    yUnit = "Rs"
    labels = list()
    data = list()
    providers = "Top 10 Products By Turnover"

    def __init__(self):
        self.populateDataAndLabels()

    def get_labels(self):
        """Return labels."""
        return self.labels

    def get_data(self):
        return self.data

    def populateDataAndLabels(self):
        top10TurnoverDataByProduct = self.getTop10TurnoverProducts()
        #pdb.set_trace()
        self.labels = list(top10TurnoverDataByProduct.keys())
        self.data = [int(x) for x in top10TurnoverDataByProduct.values()]

    def getTotalTurnoverFor(self, product):
        turnoverSum = 0
        saleOrdersForProduct = SalesOrder.objects.filter(product=product)
        for salesOrder in saleOrdersForProduct:
            turnoverSum += (salesOrder.sales_order_quantity * salesOrder.product.getSalePrice)
        return turnoverSum

    def getTop10TurnoverProducts(self):
        productTurnover = {}
        products = Product.objects.all()
        for product in products:
            productTurnover[product.product_name] = self.getTotalTurnoverFor(product)
        return {k: v for k, v in sorted(productTurnover.items(), reverse=True, key=lambda item: item[1])}

class Last30DaySaleLineGraph(BaseLineChartView):
    def __init__(self):
        self.saleData, self.cogsData, self.dateLabels = self.getLast30DaySales()
    def get_labels(self):
        """Return 7 labels for the x-axis."""
        return self.dateLabels

    def get_providers(self):
        """Return names of datasets."""
        return ["Reported Turnover", "Weighted Average Cost Of Goods Sold"]

    def get_data(self):
        """Return 3 datasets to plot."""
        return [self.saleData, self.cogsData]

    def getLast30DaySales(self):
        mostRecentSaleDate = SalesOrder.objects.order_by('-sales_order_date').first().sales_order_date
        beginDate = mostRecentSaleDate - timedelta(days=30)
        salesOrders = SalesOrder.objects.filter(sales_order_date__lte=mostRecentSaleDate, sales_order_date__gt=beginDate)\
            .order_by('-sales_order_date')
        dateLabels = list()
        saleData = list()
        costOfGoodsSoldData = list()
        saleSum = 0
        costOfGoodSoldSum = 0
        previousDate = None

        for s in salesOrders:
            currentDate = s.sales_order_date
            if previousDate is None:
                previousDate = currentDate
            if currentDate == previousDate:
                saleSum += s.product.getSalePrice * s.sales_order_quantity
                costOfGoodSoldSum += s.product.getWeightedAveragePurchasePrice * s.sales_order_quantity
            else:
                dateLabels.append(str(previousDate))
                saleData.append(saleSum)
                costOfGoodsSoldData.append(int(costOfGoodSoldSum))
                saleSum = s.product.getSalePrice * s.sales_order_quantity
                costOfGoodSoldSum = s.product.getWeightedAveragePurchasePrice * s.sales_order_quantity
                previousDate = currentDate

        if previousDate != None:
            dateLabels.append(str(previousDate))
            saleData.append(saleSum)
            costOfGoodsSoldData.append(int(costOfGoodSoldSum))
        return saleData[::-1], costOfGoodsSoldData[::-1], dateLabels[::-1]

def last30DaySaleData(request):
    turnoverData = Last30DaySaleLineGraph()
    labels = turnoverData.get_labels()
    data = turnoverData.get_data()

    return JsonResponse(data={
        'labels': labels,
        'data': data,
    })

def top10TurnoverProductBarGraph(request):
    productTurnoverData = TopTurnOverProductsBarchart()
    labels = productTurnoverData.get_labels()[:12]
    data = productTurnoverData.get_data()[:12]

    return JsonResponse(data={
        'labels': labels,
        'data': data,
        'provider' : productTurnoverData.get_providers()[0]
    })