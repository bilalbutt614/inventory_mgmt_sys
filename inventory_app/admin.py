from django.contrib import admin

from .models import Supplier, Product, Unit, ProductPurchasePrice, ProductSalePrice, ProductLocation,\
PurchaseInventory, TransferInventory, SalesOrder
# Register your models here.

admin.site.register(Supplier)
admin.site.register(Product)
admin.site.register(Unit)
admin.site.register(ProductPurchasePrice)
admin.site.register(ProductSalePrice)
admin.site.register(ProductLocation)
admin.site.register(PurchaseInventory)
admin.site.register(TransferInventory)
admin.site.register(SalesOrder)