from django.db import models
from django.urls import reverse
from django.db.models import DecimalField, Sum, ExpressionWrapper, F
from itertools import starmap
from datetime import date

# Create your models here.
import pdb

class Supplier(models.Model):
    supplier_name = models.CharField(max_length=200, unique=True)
    supplier_email = models.EmailField('e-mail', max_length = 254, blank=True)
    supplier_phone = models.CharField(max_length=32)
    supplier_address = models.CharField(max_length=256)

    class Meta:
        ordering = ['supplier_name']

    def __str__(self):
        return self.supplier_name

class Unit(models.Model):
    unit_name = models.CharField(max_length=256, unique=True, default="")
    class Meta:
        ordering = ['unit_name']
    def __str__(self):
        return self.unit_name
    
class Product(models.Model):
    supplier = models.ForeignKey(Supplier, related_name='products', on_delete=models.CASCADE)
    unit = models.ForeignKey(Unit, null = True, on_delete=models.SET_NULL)
    product_name = models.CharField(max_length=200, unique=True)
    product_minimum_order_quantity = models.IntegerField(default=0, null=True, blank=True)
    product_introduction_date = models.DateField('date introduced')
    product_description = models.CharField(max_length=1024, default ="")
    current_product_stock = models.IntegerField(default = 0)
    product_reorder_level = models.IntegerField(default = 0, null=True, blank=True)

    class Meta:
        ordering = ['product_name']

    def __str__(self):
        return self.product_name

    def get_absolute_url(self):
        return reverse('inventory:single_products', args=[self.id])

    @property
    def getWeightedAveragePurchasePrice(self):
        inventory_purchases = PurchaseInventory.objects.filter(product=self)
        total_inventory_purchases = inventory_purchases.aggregate(Sum('purchase_quantity'))
        prices_and_weights = list()
        for purchase in inventory_purchases:
            prices_and_weights.append((float(purchase.purchase_price.purchase_price_per_unit),\
                               purchase.purchase_quantity/total_inventory_purchases['purchase_quantity__sum']))
        weighted_avg_cost = sum(starmap((lambda x, y: x*y), prices_and_weights))
        return weighted_avg_cost

    @property
    def getPurchasePrice(self):
        #TODO Check if only one object of queryset is returned
        purchase_price = ProductPurchasePrice.objects.filter(product=self).latest('purchase_price_date').purchase_price_per_unit
            #ProductPurchasePrice.objects.order_by('-purchase_price_date').get(pk = self.id)\
            #.purchase_price_per_unit
        return purchase_price

    @property
    def getSalePrice(self):
        #TODO
        sale_price = ProductSalePrice.objects.filter(product=self).latest('sale_price_date').sale_price_per_unit
            #ProductSalePrice.objects.order_by('sale_price_date').get(pk=self.id).sale_price_per_unit
        return sale_price

#ProductPurchasePrice and ProductSalePrice are seemingly accounts Related models,
# may need to move to accounts application in the future
class ProductPurchasePrice(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    purchase_price_per_unit = models.DecimalField(max_digits=12, decimal_places=2)
    purchase_price_date = models.DateTimeField('date of purchase price', auto_now= True)

class ProductSalePrice(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    sale_price_per_unit = models.DecimalField(max_digits=12, decimal_places=2)
    sale_price_date = models.DateTimeField('date of sale price', auto_now= True)

class Location(models.Model):
    location_address = models.CharField(max_length=256)
    location_area_size_description = models.CharField(max_length=256, null=True, blank=True)
    class Meta:
        ordering = ['location_address']

    def __str__(self):
        return self.location_address

class ProductLocation(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    location = models.ForeignKey(Location, on_delete=models.CASCADE)
    stock_at_location = models.IntegerField(default = 0)

#Accounts Related models, may need to move to accounts application in the future

class PurchaseInventory(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    purchase_price = models.ForeignKey(ProductPurchasePrice, on_delete=models.CASCADE)
    purchase_quantity = models.IntegerField()
    purchase_date = models.DateField('date purchased', auto_now= True)

class TransferInventory(models.Model):
    product_location = models.ForeignKey(ProductLocation, on_delete=models.CASCADE)
    stock_quantity_to_transfer = models.IntegerField(default = 0)
    transfer_date = models.DateField('transfer purchased', auto_now= True)

class SalesOrder(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    sale_price = models.ForeignKey(ProductSalePrice, on_delete=models.CASCADE)
    location = models.ForeignKey(Location, on_delete=models.CASCADE)
    sales_order_quantity = models.IntegerField()
    sales_order_date = models.DateField('date introduced', auto_now= True)

    @property
    def getTodaysTotalSale(self):
        sales_orders = SalesOrder.objects.filter(sales_order_date=date.today()). \
            annotate(sale_volume=ExpressionWrapper(F('sale_price__sale_price_per_unit') * F('sales_order_quantity'),
                                                   output_field=DecimalField()))
        if sales_orders:
            total_sales_volume = sales_orders.aggregate(Sum('sale_volume'))['sale_volume__sum']
            return total_sales_volume
        return 0

    @property
    def getTodaysCostOfGoodsSold(self):
        sales_orders = SalesOrder.objects.filter(sales_order_date=date.today()). \
            annotate(sale_volume=ExpressionWrapper(F('sale_price__sale_price_per_unit') * F('sales_order_quantity'),
                                                   output_field=DecimalField()))
        total_avg_cost = 0
        if sales_orders:
            for row in sales_orders:
                total_avg_cost += row.product.getWeightedAveragePurchasePrice * row.sales_order_quantity
        return total_avg_cost

    def __str__(self):
        return self.product.product_name + ", @price: " + str(self.sale_price.sale_price_per_unit) + ", qty: " + \
               str(self.sales_order_quantity) + ", date: " + str(self.sales_order_date)
