# Generated by Django 2.2.5 on 2020-02-22 19:31

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory_app', '0016_transferinventory'),
    ]

    operations = [
        migrations.RenameField(
            model_name='product',
            old_name='product_stock',
            new_name='current_product_stock',
        ),
    ]
