# Generated by Django 2.2.5 on 2020-02-01 18:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventory_app', '0008_auto_20200201_1726'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='item_name',
            field=models.CharField(max_length=200, unique=True),
        ),
    ]
