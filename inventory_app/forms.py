from django import forms
from django.forms import formset_factory, ModelChoiceField
from .models import Unit, Supplier, Product, ProductPurchasePrice, ProductSalePrice, Location, \
    PurchaseInventory, ProductLocation, TransferInventory, SalesOrder
from bootstrap_datepicker_plus import DatePickerInput
from django.utils import timezone
import pdb

class SupplierForm(forms.ModelForm):
    class Meta:
        model = Supplier
        fields = ['supplier_name', 'supplier_email', 'supplier_phone', 'supplier_address']
    supplier_name = forms.CharField(label='Supplier name', max_length=100)
    supplier_email = forms.EmailField(label = 'e-mail', required=False)
    supplier_phone = forms.CharField(label = 'Supplier phone', max_length=32)
    supplier_address = forms.CharField(label = 'Supplier address', max_length=256)

class ProductForm(forms.Form):
    unit = forms.ModelMultipleChoiceField(queryset=Unit.objects.all())
    product_name = forms.CharField(label = 'Product name')
    product_minimum_order_quantity = forms.IntegerField(label= 'Minimum order quantity', required=False)
    product_description = forms.CharField(label ="Product description")
    product_reorder_level = forms.IntegerField(label ='Re-order level', required=False)
    product_introduction_date = forms.DateField(label='Product introduction date', widget=DatePickerInput(format='%m/%d/%Y'))
    purchase_price_per_unit = forms.DecimalField(max_digits = 12, decimal_places = 3, min_value = 0.0)
    sale_price_per_unit = forms.DecimalField(max_digits=12, decimal_places=3, min_value=0.0)

    def addProduct(self, supplier_id):
        productToAdd = self.getFormDataAsProductAndPrices(supplier_id)
        if productToAdd != None:
            prices = self.getFormDataAsPrices(productToAdd)
            prices['sale_price'].save()
            prices['purchase_price'].save()
            return True
        else:
            return False

    def getFormDataAsPrices(self, product):
        purchasePricePerUnit = self.cleaned_data['purchase_price_per_unit']
        salePricePerUnit = self.cleaned_data['sale_price_per_unit']
        sale_price = ProductSalePrice(product=product, sale_price_per_unit=salePricePerUnit)
        purchase_price = ProductPurchasePrice(product=product, purchase_price_per_unit=purchasePricePerUnit)
        return {'sale_price': sale_price, 'purchase_price':purchase_price}

    def getFormDataAsProductAndPrices(self, supplier_id):
        purchasePricePerUnit = self.cleaned_data['purchase_price_per_unit']
        salePricePerUnit = self.cleaned_data['sale_price_per_unit']
        if purchasePricePerUnit < salePricePerUnit:
            productUnit = Unit.objects.filter(unit_name=self.cleaned_data['unit'].first().unit_name).get()
            productName = self.cleaned_data['product_name']
            productMinimumOrderQuantity = self.cleaned_data['product_minimum_order_quantity']
            productIntroductionDate = self.cleaned_data['product_introduction_date']
            productDescription = self.cleaned_data['product_description']
            productStock = 0
            productReorderLevel = self.cleaned_data['product_reorder_level']
            product = Product(supplier=Supplier.objects.get(pk=supplier_id), unit=productUnit,
                              product_name=productName, product_minimum_order_quantity=productMinimumOrderQuantity,
                              product_introduction_date=productIntroductionDate, product_description=productDescription,
                              current_product_stock=productStock, product_reorder_level=productReorderLevel)
            product.save()
            for location in Location.objects.all():
                productLocation = ProductLocation(product=product, location=location, stock_at_location=0)
                productLocation.save()
            return product
        else:
            return None

class PurchaseAndSalePriceEditForm(forms.Form):
    purchase_price_per_unit = forms.DecimalField(max_digits = 12, decimal_places = 3, min_value = 0.0)
    sale_price_per_unit = forms.DecimalField(max_digits=12, decimal_places=3, min_value=0.0)

    def updatePrices(self, product):
        #TODO Check if new prices are different than current ones
        purchase_price_per_unit = self.cleaned_data['purchase_price_per_unit']
        sale_price_per_unit = self.cleaned_data['sale_price_per_unit']
        current_purchase_price = ProductPurchasePrice.objects.filter(product=product).latest('purchase_price_date').purchase_price_per_unit
        current_sale_price = ProductSalePrice.objects.filter(product=product).latest('sale_price_date').sale_price_per_unit
        if current_purchase_price != purchase_price_per_unit:
            product_purchase_price = ProductPurchasePrice(product=product, purchase_price_per_unit=purchase_price_per_unit)
            product_purchase_price.save()
        if current_sale_price != sale_price_per_unit:
            product_sale_price = ProductSalePrice(product=product,sale_price_per_unit=sale_price_per_unit)
            product_sale_price.save()
        #ProductPurchasePrice.objects.filter(product=product).update(purchase_price_per_unit=purchase_price_per_unit)
        #ProductSalePrice.objects.filter(product=product).update(sale_price_per_unit=sale_price_per_unit)

class UnitForm(forms.ModelForm):
    class Meta:
        model = Unit
        fields = ['unit_name']
    unit_name = forms.CharField(label='Unit name')

class ProductSupplierForm(ProductForm):
    supplier = forms.ModelMultipleChoiceField(queryset=Supplier.objects.all())
    def getSupplierFromFormData(self):
        supplier = Supplier.objects.filter(supplier_name=self.cleaned_data['supplier'].first()).get()
        return supplier

    def addProductWithSupplier(self):
        supplier = self.getSupplierFromFormData()
        return super().addProduct(supplier.id)

class LocationForm(forms.Form):
    location_address = forms.CharField(label = 'Locations for inventory')
    location_area_size_description = forms.CharField(label = 'Description of location size')

    def addLocation(self):
        locationAddress = self.cleaned_data['location_address']
        locationAreaSizeDescription = self.cleaned_data['location_area_size_description']
        location = Location(location_address=locationAddress, location_area_size_description=locationAreaSizeDescription)
        #TODO Error checking
        location.save()
        for product in Product.objects.all():
            productLocation = ProductLocation(product = product, location = location, stock_at_location = 0)
            productLocation.save()
        return True

class ProductPurchasePriceChoiceField(ModelChoiceField):
    def label_from_instance(self, obj):
        return '{productName} (Rs.{purchasePrice} per {unit})'.format(productName=obj.product_name,
                                                                unit = obj.unit,
                                                                purchasePrice = int(obj.getPurchasePrice))

class ProductSalePriceChoiceField(ModelChoiceField):
    def label_from_instance(self, obj):
        return '{productName} (Rs.{salePrice} per {unit})'.format(productName=obj.product_name,
                                                                unit = obj.unit,
                                                                salePrice = int(obj.getSalePrice))

class PurchaseInventoryForm(forms.Form):
    product = ProductPurchasePriceChoiceField(label='Product', queryset=Product.objects.all())
    purchase_quantity = forms.IntegerField(label ='Purchase quantity', required=True)

    def addPurchase(self):
        product = self.cleaned_data['product']
        purchase_quantity = self.cleaned_data['purchase_quantity']
        purchase_price = ProductPurchasePrice.objects.filter(product = product).latest("purchase_price_date")
        purchase_inventory = PurchaseInventory(product=product, purchase_quantity=purchase_quantity, \
                                               purchase_price=purchase_price)
        current_quantity = Product.objects.get(pk=product.id).current_product_stock
        Product.objects.filter(pk=product.id).update(current_product_stock=current_quantity + purchase_quantity)
        purchase_inventory.save()

class TransferInventoryForm(forms.Form):
    product = ProductPurchasePriceChoiceField(label='Product', queryset=Product.objects.all())
    location = ModelChoiceField(label='Location', queryset=Location.objects.all())
    transfer_quantity = forms.IntegerField(label ='Transfer quantity', required=True)

    def checkIsQuantityTransferable(self, product, quantity_to_transfer):
        locations = Location.objects.all()
        total_available_quantity = product.current_product_stock
        quantity_sum = 0
        for location in locations:
            product_location = ProductLocation(product=product, location=location)
            quantity_sum += product_location.stock_at_location
        return (quantity_sum + quantity_to_transfer) <= total_available_quantity

    def transferInventory(self):
        product = self.cleaned_data['product']
        location = self.cleaned_data['location']
        product_location = ProductLocation.objects.get(product=product, location=location)
        quantity_to_transfer = self.cleaned_data['transfer_quantity']
        if self.checkIsQuantityTransferable(product, quantity_to_transfer) is True:
            transfer_date = timezone.now()
            transfer_inventory = TransferInventory(product_location=product_location, \
                                               stock_quantity_to_transfer=quantity_to_transfer,\
                                                   transfer_date=transfer_date)
            transfer_inventory.save()
            current_quantity_at_location = product_location.stock_at_location
            product_location.stock_at_location = current_quantity_at_location + quantity_to_transfer
            product_location.save()
            return True
        return False

class SalesOrderForm(forms.ModelForm):
    class Meta:
        model = SalesOrder
        fields = ['product', 'sales_order_quantity']
    product = ProductSalePriceChoiceField(label='Product', queryset=Product.objects.all())
    sales_order_quantity = forms.IntegerField(label='Sales order quantity', required=True)

    def createSaleOfInventory(self, location):
        product = self.cleaned_data['product']
        sales_order_quantity = self.cleaned_data['sales_order_quantity']
        sale_price = ProductSalePrice.objects.filter(product = product).latest("sale_price_date")
        sale_order = SalesOrder(product=product, location=location, sales_order_quantity=sales_order_quantity,
                                sale_price = sale_price)
        product_location = ProductLocation.objects.get(product=product, location=location)
        if sales_order_quantity <= product_location.stock_at_location:
            product_location.stock_at_location= product_location.stock_at_location - sales_order_quantity
            product.current_product_stock = product.current_product_stock - sales_order_quantity
            sale_order.save()
            product.save()
            product_location.save()
            return True
        return 'Sale quantity of {saleQty} at location {location} exceeds the stock ({stock_at_location}) ' \
               'present at the location'.format(saleQty= sales_order_quantity, location=location.location_address,
                                                                stock_at_location = product_location.stock_at_location)

class InventoryFormsets:
    @staticmethod
    def getPurchaseInventoryFormSet():
        PurchaseInventoryFormSet = formset_factory(
            PurchaseInventoryForm,
            extra=0,
            max_num=len(Product.objects.all()),
            min_num=1
        )
        return PurchaseInventoryFormSet

    @staticmethod
    def getTransferInventoryFormSet():
        TransferInventoryFormSet = formset_factory(
            TransferInventoryForm,
            extra=0,
            max_num=len(Product.objects.all()) * len(Location.objects.all()),
            min_num=1
        )
        return TransferInventoryFormSet

    @staticmethod
    def getSalesOrderFormSet():
        SalesOrderFormSet = formset_factory(
            SalesOrderForm,
            extra=0,
            max_num=len(Product.objects.all()),
            min_num=1
        )
        return SalesOrderFormSet