from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.views.generic import ListView, UpdateView, View, RedirectView
from .models import Supplier, Product, Location, PurchaseInventory, TransferInventory, ProductLocation,\
Unit, SalesOrder
from .forms import SupplierForm, ProductForm, ProductSupplierForm, LocationForm, \
    PurchaseInventoryForm, TransferInventoryForm, InventoryFormsets, PurchaseAndSalePriceEditForm,\
    UnitForm, SalesOrderForm
from django.urls import reverse, reverse_lazy
from django.views.generic.edit import CreateView, FormView
from datetime import date
from django.db.models import DecimalField, Sum, ExpressionWrapper, F
from datetime import timedelta

# Create your views here.
#FOR DEBUGGING PURPOSES
import pdb

class HomeRedirectView(RedirectView):
  url = reverse_lazy('inventory:index')

class DashboardView(LoginRequiredMixin, View):
    template_name = 'inventory_app/dashboard.html'

    def getTop10HighestMarginProducts(self):
        productMargins = [(product.product_name, float(product.getSalePrice) - product.getWeightedAveragePurchasePrice,
                           100*((float(product.getSalePrice) - product.getWeightedAveragePurchasePrice)/float(product.getSalePrice)))
                          for product in Product.objects.all()]
        return (sorted(productMargins, key=lambda tup: tup[1], reverse=True))[:10]

    def get(self, request, *args, **kwargs):
        #pdb.set_trace()
        productMargins = self.getTop10HighestMarginProducts()
        todaysSale = SalesOrder().getTodaysTotalSale
        todaysCOGS = SalesOrder().getTodaysCostOfGoodsSold
        todaysGrossProfit = float(todaysSale) - todaysCOGS
        context = { 'daily_sale' : todaysSale,
                    'daily_cost_of_goods_sold' : todaysCOGS,
                    'daily_gross_profit' : todaysGrossProfit,
                    'product_margins': productMargins}
        return render(request, self.template_name, context)

class SupplierView(LoginRequiredMixin, ListView):
    template_name = 'inventory_app/supplier/supplier.html'
    context_object_name = 'supplier_list'
    paginate_by = 6
    def get_queryset(self):
        return Supplier.objects.all()

class ProductSupplierView(LoginRequiredMixin, ListView):
    template_name = 'inventory_app/supplier/product_supplier.html'
    context_object_name = 'product_supplier_list'
    paginate_by = 6

    def get_queryset(self):
        supplier_id = self.kwargs['supplier_id']
        supplier_name = Supplier.objects.get(pk=self.kwargs['supplier_id']).supplier_name
        supplier_product = (supplier_id, supplier_name, Supplier.objects.get(pk=self.kwargs['supplier_id']).products.all())
        return supplier_product

class ProductView(LoginRequiredMixin, ListView):
    template_name = 'inventory_app/product/product.html'
    context_object_name = 'product_list'
    paginate_by = 10
    product_id = None

    def get_queryset(self):
        if ('product_id' in self.kwargs):
            self.product_id = self.kwargs['product_id']
            found_product = get_object_or_404(Product, pk = self.product_id)
            return [found_product]
        return Product.objects.all()

class AddSupplierView(LoginRequiredMixin, CreateView):
    model = Supplier
    template_name = 'inventory_app/supplier/add_supplier.html'
    form_class = SupplierForm
    success_url = reverse_lazy('inventory:supplier')

    def form_valid(self, form):
        return super().form_valid(form)

class AddUnitView(LoginRequiredMixin, CreateView):
    model = Unit
    template_name = 'inventory_app/product/add_unit.html'
    form_class = UnitForm
    success_url = reverse_lazy('inventory:multiple_products')

    def form_valid(self, form):
        return super().form_valid(form)

class AddProductView(LoginRequiredMixin, CreateView):
    template_name = 'inventory_app/product/add_product.html'
    form_class = ProductForm
    success_url = reverse_lazy('inventory:multiple_products')

    def get(self, request, *args, **kwargs):
        if 'supplier_id' in kwargs:
            context = {'form': ProductForm(), 'supplier_id': self.kwargs['supplier_id']}
        else:
            context = {'form':  ProductSupplierForm(), 'supplier_id': None}
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        if 'supplier_id' in kwargs:
            form = ProductForm(request.POST)
            supplier_id = self.kwargs['supplier_id']
            if form.is_valid():
                if form.addProduct(supplier_id) is True:
                    return HttpResponseRedirect(reverse('inventory:product_supplier', args=([supplier_id])))
            return render(request, self.template_name, context= {'supplier_id' : supplier_id,'form': form})
        else:
            form = ProductSupplierForm(request.POST)
            if form.is_valid():
                if form.addProductWithSupplier() is True:
                    return HttpResponseRedirect(self.success_url)
            return render(request, self.template_name, context={'form': form})

class EditProductPricesView(LoginRequiredMixin, UpdateView):
    model = Product
    form_class = PurchaseAndSalePriceEditForm
    template_name = 'inventory_app/product/edit_product_prices.html'
    context_object_name = 'editable_product'
    success_url = reverse_lazy('inventory:multiple_products')

    def get_context_data(self, **kwargs):
        product_id = self.kwargs['product_id']
        product = get_object_or_404(Product, pk=product_id)
        product_sale_price = product.getSalePrice
        product_purchase_price = product.getPurchasePrice
        context = {'product': product, 'product_sale_price': product_sale_price,
                   'product_purchase_price': product_purchase_price}
        return context

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        intitial_data = {'purchase_price_per_unit': context['product_purchase_price'],
                          'sale_price_per_unit' : context['product_sale_price']}
        form = PurchaseAndSalePriceEditForm(request.GET or None, initial=intitial_data)
        context['form'] = form
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        form = PurchaseAndSalePriceEditForm(request.POST or None)
        product_id = self.kwargs['product_id']
        product = get_object_or_404(Product, pk=product_id)
        if form.is_valid():
            form.updatePrices(product)
            return HttpResponseRedirect(self.success_url)
        return render(request, self.template_name, self.context)

class LocationView(LoginRequiredMixin, FormView):
    template_name = 'inventory_app/location/location.html'
    form_class = LocationForm
    success_url = reverse_lazy('inventory:locations')

    def get(self, request, *args, **kwargs):
        current_locations = Location.objects.all()
        context = {'form': LocationForm(), 'locations': current_locations}
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        form = LocationForm(request.POST)
        if form.is_valid():
            if form.addLocation() is True:
                return HttpResponseRedirect(self.success_url)
        return render(request, self.template_name, self.context)

class ProducLocationView(LoginRequiredMixin, ListView):
    template_name = 'inventory_app/location/product_location.html'
    context_object_name = 'product_stock'
    paginate_by = 8
    def get_queryset(self):
        location_id = self.kwargs['location_id']
        location = Location.objects.get(pk=self.kwargs['location_id'])
        products_at_location = ProductLocation.objects.filter(location=location)
        product_stock = [(prod.product.product_name, prod.product.unit.unit_name,
                          prod.stock_at_location) for prod in products_at_location]
        return product_stock

class PurchaseInventoryView(LoginRequiredMixin, FormView):
    template_name = 'inventory_app/manage_inventory/purchase_inventory.html'
    model = PurchaseInventory
    form_class = PurchaseInventoryForm
    success_url = reverse_lazy('inventory:multiple_products')
    intitial_data = [{'purchase_quantity': 0}]

    def get(self, request, *args, **kwargs):
        formset = InventoryFormsets.getPurchaseInventoryFormSet()
        purchase_inventory_form = formset(request.GET or None, initial=self.intitial_data)
        context = {'formset': purchase_inventory_form}
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        formset = InventoryFormsets.getPurchaseInventoryFormSet()
        purchase_inventory_form = formset(request.POST, initial=self.intitial_data)
        if purchase_inventory_form.is_valid():
            for purchase_form in purchase_inventory_form:
                purchase_form.addPurchase()
            return HttpResponseRedirect(self.success_url)
        #TODO Error scenario
        return HttpResponseRedirect(self.success_url)

class TransferInventoryView(LoginRequiredMixin, FormView):
    template_name = 'inventory_app/manage_inventory/transfer_inventory.html'
    model = TransferInventory
    form_class = TransferInventoryForm
    success_url = reverse_lazy('inventory:locations')
    intitial_data = [{'purchase_quantity': 0}]

    def get(self, request, *args, **kwargs):
        formset = InventoryFormsets.getTransferInventoryFormSet()
        purchase_inventory_form = formset(request.GET or None, initial=self.intitial_data)
        context = {'formset': purchase_inventory_form}
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        formset = InventoryFormsets.getTransferInventoryFormSet()
        transfer_inventory_form = formset(request.POST or None, initial=self.intitial_data)
        if transfer_inventory_form.is_valid():
            for transfer_form in transfer_inventory_form:
                transfer_form.transferInventory()
            return HttpResponseRedirect(self.success_url)
        #TODO Error scenario
        return HttpResponseRedirect(self.success_url)

class SaleInventoryView(LoginRequiredMixin, CreateView):
    model = SalesOrder
    template_name = 'inventory_app/manage_inventory/sale_of_inventory.html'
    form_class = SalesOrderForm
    success_url = reverse_lazy('inventory:index')

    def get(self, request, *args, **kwargs):
        locations = Location.objects.all()
        formset = InventoryFormsets.getSalesOrderFormSet()
        sale_of_inventory_form = formset(request.GET or None)
        context = {'formset': sale_of_inventory_form, 'locations' : locations}
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        formset = InventoryFormsets.getSalesOrderFormSet()
        sales_order_inventory_form = formset(request.POST or None)
        location = get_object_or_404(Location, pk=request.POST['location'])
        if sales_order_inventory_form.is_valid():
            sales_order_success = True
            for sales_order_form in sales_order_inventory_form:
                if sales_order_form.createSaleOfInventory(location) is not True:
                    sales_order_success = False
            if sales_order_success is True:
                return HttpResponseRedirect(self.success_url)
            else:
                pass
                # TODO Error scenario
        return HttpResponseRedirect(self.success_url)

class DailySaleView(LoginRequiredMixin, ListView):
    template_name = 'inventory_app/sales_reports/daily_sale.html'
    paginate_by = 6
    #TODO Fix pagination for DailySaleView
    def get(self, request, *args, **kwargs):
        sales_orders = SalesOrder.objects.filter(sales_order_date=date.today()). \
            annotate(sale_volume=ExpressionWrapper(F('sale_price__sale_price_per_unit') * F('sales_order_quantity'),
                                                   output_field=DecimalField()))
        if sales_orders:
            total_sales_volume = sales_orders.aggregate(Sum('sale_volume'))['sale_volume__sum']
            total_avg_cost = 0
            for row in sales_orders:
                total_avg_cost += row.product.getWeightedAveragePurchasePrice *row.sales_order_quantity
            gross_profit = float(total_sales_volume) - total_avg_cost
            context = {'sales_orders': sales_orders, 'todays_date' : date.today(), \
                       'total_sales_volume' : total_sales_volume, 'total_avg_cost' : total_avg_cost, \
                       'gross_profit' : gross_profit}
        else:
            context = {'sales_orders': None}
        return render(request, self.template_name, context)

class MonthlyPurchaseView(LoginRequiredMixin, ListView):
    template_name = 'inventory_app/reports/monthly_purchase.html'
    context_object_name = 'purchaseOrders'
    paginate_by = 8

    def get_queryset(self):
        mostRecentPurchaseDate = PurchaseInventory.objects.order_by('-purchase_date').first().purchase_date
        beginDate = mostRecentPurchaseDate - timedelta(days=30)
        purchaseOrders = PurchaseInventory.objects.filter(purchase_date__lte=mostRecentPurchaseDate,
                                                          purchase_date__gt=beginDate) \
            .order_by('-purchase_date')
        return purchaseOrders