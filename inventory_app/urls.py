from django.urls import path
from . import views
from . import charts

app_name = 'inventory'
urlpatterns = [
    #index
    path('', views.HomeRedirectView.as_view()),
    path('home', views.DashboardView.as_view(), name='index'),
    #supplier
    path('supplier/', views.SupplierView.as_view(), name='supplier'),
    path('add_supplier/', views.AddSupplierView.as_view(), name='add_supplier'),
    path('<int:supplier_id>/item_supplier/', views.ProductSupplierView.as_view(), name='product_supplier'),
    path('<int:supplier_id>/item_supplier/add_product/', views.AddProductView.as_view(), name='add_product'),
    #product
    path('product/', views.ProductView.as_view(), name='multiple_products'),
    path('product/add_product', views.AddProductView.as_view(), name='add_product_with_supplier'),
    path('<int:product_id>/product/', views.ProductView.as_view(), name='single_products'),
    path('<int:product_id>/edit_prices', views.EditProductPricesView.as_view(), name='edit_prices'),
    path('product/add_unit', views.AddUnitView.as_view(), name='add_unit'),
    #location
    path('locations/', views.LocationView.as_view(), name='locations'),
    path('<int:location_id>/product_locations/', views.ProducLocationView.as_view(), name='product_locations'),
    #inventory management urls
    path('purchase_inventory/', views.PurchaseInventoryView.as_view(), name='purchase_inventory'),
    path('transfer_inventory/', views.TransferInventoryView.as_view(), name='transfer_inventory'),
    path('sale_of_inventory/', views.SaleInventoryView.as_view(), name='sale_of_inventory'),
    #reports
    path('daily_sale_report/', views.DailySaleView.as_view(), name='daily_sale_report'),
    path('monthly_purchase_report/', views.MonthlyPurchaseView.as_view(), name='monthly_purchase_report'),
    #charts
    path('top10TurnoverProductBarGraph/', charts.top10TurnoverProductBarGraph, name='top10TurnoverProductBarGraph'),
    path('Last30DaySaleLineGraph/', charts.Last30DaySaleLineGraph.as_view(), name='Last30DaySaleLineGraph'),
]